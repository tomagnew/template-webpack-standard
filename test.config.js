var path = require('path');

module.exports = {
  entry: './test', // all test files to be run must be required in test/index.js
  output: {
    path: path.resolve(__dirname, 'test'),
    filename: 'test.bundle.js' // compiled bundle of all tests
  }
};