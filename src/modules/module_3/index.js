export default {
    sayMyName(name) {
        return `My name is ${name}`;
    }
}