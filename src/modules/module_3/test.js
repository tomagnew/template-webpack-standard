import {assert} from 'chai'; 
import module_3 from './index';

describe('Module_3 basic tests', () => {

    it('should return "My name is Tom"', () => {
        assert.equal(module_3.sayMyName("Tom"), "My name is Tom");
    });

});