console.log('Sample unit tests running!');

// set up tests in each file like:
var assert = require('chai').assert; // choose assertion library
// put like-minded tests inside of describe blocks
describe('Sample basic tests', () => {
    // each 'it' function defines one test or 'spec'. An 'it' can contain multiple asserts.
    it('Foo should equal foo', () => {
        assert.equal("foo", "foo");
        assert.notEqual("foo", "bar");
    });
    it('Foo should not equal baz', () => {
        assert.notEqual("foo", "baz");
    });
});

import person from './person';
console.log(person);

var assert = require('chai').assert;

describe('Sample person tests', () => {
    it('person.name should equal tom', () =>
        assert.equal(person.name, "Tom")
    );
});